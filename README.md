Vue2.js + Webpack
===================


MediaMonks' Vue2.js + Webpack starter pack.
Created by [Peter Altamirano](https://twitter.com/peteraltamiran0) and [Armand Biteau](https://twitter.com/armandbiteau)

----------

> **Stack:**

> - **ES6** (Babel + eslint)
> - **SCSS** (autoprefixer + url rewriting)
> - **FLOW** (type checking)
> - File **versionning** (js + css)

----------

Frontend process
-------------

#### Installation
```
yarn
```
or
```
npm install
```
Update package.json's title and description (automatically injected in html templates)

#### Development
```
npm run serve
```
The project will run under http://localhost:3000

#### Build for production
```
npm run build
```
Builds the project into **/deploy/htdocs**

> **Note:** To build the project with a different public path (root in subfolder for instance), update PUBLIC_PATH in config/config.js

----------

Resources
-------------

> **Note:** You can find more information about:

> - **Vue.js** [https://vuejs.org/](https://vuejs.org/),
> - **Webpack** [https://webpack.github.io/](https://webpack.github.io/),
> - **Flow** [https://flowtype.org/](https://flowtype.org/)
> - **Favicons** [http://realfavicongenerator.net](http://realfavicongenerator.net)
