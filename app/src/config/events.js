const events = {

    /*
    * WINDOW
    */

    WINDOW_RESIZE: 'WINDOW_RESIZE',

    IS_LOADED: 'IS_LOADED',

    /*
    * ROUTER
    */

    ROUTER_ROUTE_CHANGE: 'ROUTER_ROUTE_CHANGE'

    /*
    * APPLICATION
    */

};

module.exports = events;
