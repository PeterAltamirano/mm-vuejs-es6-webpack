const LOCALS = {
    en: {
        general: {
            tweet: 'Twitter tweet'
        },
        message: {
            hello: 'hello world'
        }
    },
    ja: {
        general: {
            tweet: 'こんにちは こんにちはこんにちは こんにちは'
        },
        message: {
            hello: 'こんにちは、世界'
        }
    }
};

module.exports = LOCALS;
