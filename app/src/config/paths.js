import {
    PUBLIC_PATH_HOST
} from '../../../config';

export const SERVER_URL = window.location.origin || window.location.protocol + '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '');

export const ROOT_URL = __DEV__ ? SERVER_URL : SERVER_URL + PUBLIC_PATH_HOST;

export const PATHS = {

    PUBLIC_PATH_HOST: PUBLIC_PATH_HOST,

    PATH_ROOT: ROOT_URL,

    PATH_IMAGES: ROOT_URL + '/images',

    PATH_MEDIA: ROOT_URL + '/media',

    PATH_DATAS: ROOT_URL + '/datas'

};

module.exports = PATHS;
