// @flow

import 'styles/global.scss';

import Application from 'containers/Application/application.vue';

import Router from 'core/Router';

import domready from 'domready';

class Main {

    constructor() {

        this.addEventListeners();

        this.start();

    }

    addEventListeners() {

    }

    start() {

        new Vue({
            router: Router,
            el: '#application',
            render(h) {
                return h(Application);
            }
        });

    }
}

domready(() => {

    new Main();

});
