import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VueI18n from 'vue-i18n';

import Emitter from 'core/Emitter';

import IntroContainer from 'containers/Intro/intro.vue';
import ExampleContainer from 'containers/Example/example.vue';

import Locals from 'config/locals';

import {
    PUBLIC_PATH_HOST, PATH_ROOT
} from 'config/paths';

import {
    ROUTER_ROUTE_CHANGE
} from 'config/events';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueI18n);

// set default language
Vue.config.lang = 'en';

// load all the locals
Object.keys(Locals).forEach(function (lang) {
    Vue.locale(lang, Locals[lang]);
});

class Router extends VueRouter {

    constructor() {

        let opts = {
            pushState: true,
            mode: 'history',
            abstract: false,
            scrollBehavior: (to, from, savedPosition) => {
                return savedPosition || { x: 0, y: 0 };
            },
            routes: [{
                path: '/',
                name: 'intro',
                title: 'Homepage',
                component: IntroContainer
            }, {
                path: '/example',
                name: 'example',
                title: 'Example Route',
                component: ExampleContainer
            }],
            path: '/',
            firstRoute: true,
            routeTimeout: null
        };

        // check URI if the app is running in subfolder
        if (PUBLIC_PATH_HOST !== '/' && PATH_ROOT.indexOf(PUBLIC_PATH_HOST) > -1) {
            opts.root = PUBLIC_PATH_HOST;
        }

        super(opts);

        this.isLoaded = false;

        this.beforeEach((to, from, next) => {

            if (!to.matched.length) {
                next('/');
            }

            Emitter.$emit(ROUTER_ROUTE_CHANGE, {
                to: to,
                from: from
            });

            next();
        });

        this.afterEach(() => {

            window.scrollTo(0, 0);

        });
    }
}

export default new Router();
