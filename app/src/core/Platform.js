/**
 * Platform
 * @namespace platform
 * @description Device / Browser checker
 */

import MobileDetect from 'mobile-detect';
import browser from 'detect-browser';

class Platform {

    constructor() {

        this.userAgent = window.navigator.userAgent;

        this.mobileDetect = new MobileDetect(this.userAgent);

        this.browserName = browser.name;

        this.deviceType = this.mobileDetect.mobile() ? 'mobile' : this.mobileDetect.tablet() ? 'tablet' : 'desktop';

    }

    /*
    * Device
    */

    /**
     * @memberof platform
     * @function isMobile
     */
    isMobile() {
        return this.mobileDetect.mobile();
    }
    /**
     * @memberof platform
     * @function isTablet
     */
    isTablet() {
        return this.mobileDetect.tablet();
    }
    /**
     * @memberof platform
     * @function isDesktop
     */
    isDesktop() {
        return this.mobileDetect.desktop();
    }
    /**
     * @memberof platform
     * @function isAndroid
     */
    isAndroid() {
        return navigator.userAgent.toLowerCase().indexOf('android') > -1;
    }
    /**
     * @memberof platform
     * @function isIOS
     */
    isIOS() {
        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
    }

    /*
    * Browser
    */

    /**
     * @memberof platform
     * @function isIE
     * @returns {Bool}
     */
    isIE(): bool {
        return (this.browserName === 'edge' || this.browserName === 'ie');
    }
    /**
     * @memberof platform
     * @function isSafari
     * @returns {Bool}
     */
    isSafari(): bool {
        return (this.browserName === 'safari');
    }
    /**
     * @memberof platform
     * @function isChrome
     * @returns {Bool}
     */
    isChrome(): bool {
        return (this.browserName === 'chrome');
    }
    /**
     * @memberof platform
     * @function isFirefox
     * @returns {Bool}
     */
    isFirefox(): bool {
        return (this.browserName === 'firefox');
    }
    /**
     * @memberof platform
     * @function isOpera
     * @returns {Bool}
     */
    isOpera(): bool {
        return (this.browserName === 'opera');
    }

}


export default new Platform();
