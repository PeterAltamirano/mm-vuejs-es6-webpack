import Store from 'store';

class Data {

    constructor() {
        this.data = Store.state.data;
    }

    load(url): any {
        let promise = Vue.http.get(url);

        promise.then((data) => {
            Store.commit('SET_DATA', { data: data.data });
        });

        return promise;
    }
}

export default new Data();
