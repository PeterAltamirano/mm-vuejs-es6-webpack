import Transitions from 'transitions';

class Transition extends Transitions.master {

    // Use default transition
    constructor() {
        super();
    }

}

export default (new Transition());
