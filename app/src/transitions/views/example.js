import Transitions from 'transitions';

import {
    TimelineMax
} from 'gsap';

class Transition extends Transitions.master {

    constructor() {
        super();
    }

    // Views animations are called from 'Application', without params. 
    // override parameters if 1 exists
    enter(el, params, done) {

        params = Object.assign(this._params.enter, params);

        let _tl = new TimelineMax({
            paused: true,
            delay: params.delay,
            onComplete: () => {
                done();
            }
        });

        _tl.fromTo(el, params.speed, {
            alpha: 0
        }, {
            alpha: 1,
            ease: params.ease,
            clearProps: 'all'
        }, '+=0');

        _tl.addCallback(() => {

            Transitions.partial('slideUp').enter(el.querySelector('.a-directive'), {
                speed: 0.7,
                delay: 1.0,
                ease: Power2.easeInOut
            }, () => {});

        }, 0, []);

        _tl.play();

    }

}

export default (new Transition());
