import {
    TweenLite
} from 'gsap';

import Transitions from 'transitions';

class Transition extends Transitions.master {

    constructor() {
        super();
    }

    // same use as 'transitions/components'
    // a partial animation can be used directly in a component/view transition, either from the Vue App
    enter(el, params, done) {

        params = Object.assign(this._params.enter, params);

        TweenLite.fromTo(el, params.speed, {
            css: {
                'opacity': '0',
                'transform': 'translateY(50px)'
            }
        }, {
            css: {
                'opacity': '1',
                'transform': 'translateY(0)'
            },
            clearProps: 'all',
            delay: params.delay,
            ease: params.ease,
            onComplete: () => {
                done();
            }
        });

    }

}

export default (new Transition());
