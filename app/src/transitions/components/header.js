import {
    TweenLite
} from 'gsap';

import Transitions from 'transitions';

class Transition extends Transitions.master {

    constructor() {
        super();
    }

    // Can be called from anywhere. Assign default params values.
    enter(el, params, done) {

        params = Object.assign(this._params.enter, params);

        TweenLite.fromTo(el, params.speed, {
            css: {
                'transform': 'translateY(-100px)'
            }
        }, {
            css: {
                'transform': 'translateY(0)'
            },
            clearProps: 'all',
            delay: params.delay,
            ease: params.ease,
            onComplete: () => {
                done();
            }
        });

    }


}

export default (new Transition());
