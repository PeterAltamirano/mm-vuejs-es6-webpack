export default {

    master: require('transitions/_master/master.js'),

    view: (name: string) => {

        try {
            return require(`transitions/views/${name}.js`);
        } catch (e) {
            return require('transitions/_master/instance.js');
        }
        
    },

    component: (name: string) => {
        try {
            return require(`transitions/components/${name}.js`);
        } catch (e) {
            return require('transitions/_master/instance.js');
        }
    },

    partial: (name: string) => {
        try {
            return require(`transitions/partials/${name}.js`);
        } catch (e) {
            return require('transitions/_master/instance.js');
        }
    }

};
