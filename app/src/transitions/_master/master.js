import {
    TweenLite
 } from 'gsap';

class Transition {

    constructor() {

        this._params = {
            enter: {
                delay: 0,
                speed: 0.5,
                ease: Linear.easeNone
            },
            leave: {
                delay: 0,
                speed: 0.5,
                ease: Linear.easeNone
            }
        };

    }
      
    beforeEnter() {
    }

    enter(el: any, params: Object = {}, callback: Function) {

        params = Object.assign(this._params.enter, params);

        TweenLite.fromTo(el, params.speed, {
            alpha: 0
        }, {
            alpha: 1,
            delay: params.delay,
            ease: params.ease,
            clearProps: 'all',
            onComplete: () => {
                callback();
            }
        });
        
    }

    afterEnter() {
    }

    enterCancelled() {
    }
    
    beforeLeave() {
    }

    leave(el: any, params: Object = {}, callback: Function) {

        params = Object.assign(this._params.leave, params);

        TweenLite.fromTo(el, params.speed, {
            alpha: 1
        }, {
            alpha: 0,
            delay: params.delay,
            ease: params.ease,
            clearProps: 'all',
            onComplete: () => {
                callback();
            }
        });

    }

    afterLeave() {
    }

    leaveCancelled() {
    }

}

export default Transition;
