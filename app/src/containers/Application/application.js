// @flow

import style from './application.scss';
import EventManagerMixin from 'mixins/EventManagerMixin';
import ExampleMixin from 'mixins/ExampleMixin';

import Store from 'store';

import Transitions from 'transitions';

import Loader from 'components/Loader/loader.vue';
import Header from 'components/Header/header.vue';
import Sharing from 'components/Sharing/sharing.vue';

import {
    WINDOW_RESIZE, IS_LOADED, ROUTER_ROUTE_CHANGE
} from 'config/events';

export default Vue.extend({

    mixins: [
        ExampleMixin,
        EventManagerMixin
    ],

    appEvents: [{
        message: IS_LOADED,
        method: 'onLoaded'
    }, {
        message: ROUTER_ROUTE_CHANGE,
        method: 'onRouteChange'
    }],

    domEvents: [{
        target: window,
        event: 'resize',
        method: 'onResize'
    }],

    data() {

        let isLoaded: bool = false;
        let deviceClass: string = Store.state.device + '-device';
        let browserClass: string = Store.state.browser + '-browser';

        return {
            style,
            isLoaded,
            deviceClass,
            browserClass
        };

    },

    computed: {

        route(): string {
            return Store.state.application.route.current;
        },

        previousRoute(): string {
            return Store.state.application.route.previous;
        }

    },

    created() {

        this.bind();

    },

    mounted() {

        this.$nextTick(() => {

        });

    },

    methods: {

        /*
        * UTILS METHODS
        */

        bind() {
            Store.dispatch('BIND_ROUTE');
        },

        /*
        * APP METHODS
        */

        onLoaded() {
            this.$router.isLoaded = true;
            this.isLoaded = true;
        },

        onResize() {
            this.emitter.$emit(WINDOW_RESIZE);
        },

        /*
        * LOCAL EVENTS
        */

        onRouteChange(route) {
            Store.dispatch('CHANGE_ROUTE', route);
        },

        /**
         * VIEWS TRANSITIONS
         */

        beforeEnter(el) {
            Transitions.view(this.route).beforeEnter(el);
        },
        enter(el, done) {
            Transitions.view(this.route).enter(el, {}, done);
        },
        afterEnter(el) {
            Transitions.view(this.route).afterEnter(el);
        },
        enterCancelled(el) {
            Transitions.view(this.route).enterCancelled(el);
        },
        beforeLeave(el) {
            Transitions.view(this.previousRoute).beforeLeave(el);
        },
        leave(el, done) {
            Transitions.view(this.previousRoute).leave(el, {}, done);
        },
        afterLeave(el) {
            Transitions.view(this.previousRoute).afterLeave(el);
        },
        leaveCancelled(el) {
            Transitions.view(this.previousRoute).leaveCancelled(el);
        }

    },

    components: {
        'loader-component': Loader,
        'header-component': Header,
        'sharing-component': Sharing
    },

    partials: {

    }

});
