import style from './example.scss';
import EventManagerMixin from 'mixins/EventManagerMixin';

export default Vue.extend({

    mixins: [EventManagerMixin],

    appEvents: [],

    domEvents: [],

    props: {

    },

    data() {

        let dirValue: number = 0;

        return {
            style,
            dirValue
        };

    },

    computed: {

    },

    created() {

    },

    mounted() {
        this.$nextTick(() => {
            window.setInterval(() => {
                this.dirValue++;
            }, 1000);
        });
    },

    methods: {

    },

    components: {

    },

    directives: {

    }

});
