import style from './intro.scss';
import EventManagerMixin from 'mixins/EventManagerMixin';

import {
    PATH_ROOT
} from 'config/paths';

import {
    WINDOW_RESIZE
} from 'config/events';

export default Vue.extend({

    mixins: [EventManagerMixin],

    domEvents: [],

    appEvents: [{
        message: WINDOW_RESIZE,
        method: 'onResize'
    }],

    props: {

    },

    data() {

        let root: string = PATH_ROOT;

        return {
            style,
            root
        };

    },

    computed: {

    },

    created() {

    },

    mounted() {
        this.$nextTick(() => {

        });
    },

    methods: {

        onResize() {
            console.log('intro: resize event');
        }

    },

    components: {

    },

    directives: {
        'example-directive': require('directives/ExampleDirective/index.js')
    }

});
