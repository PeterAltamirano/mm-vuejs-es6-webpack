export default Vue.directive('example-directive', {

    bind: function () {

    },

    // update: function (el, binding, vnode)
    update: function (el, binding) {
        el.innerHTML = binding.value;
    }

});
