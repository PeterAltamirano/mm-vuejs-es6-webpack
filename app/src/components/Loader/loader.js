// @flow
import style from './loader.scss';
import EventManagerMixin from 'mixins/EventManagerMixin';
import PreloadJs from 'preload-js';
import Data from 'core/Data';

import {
    TweenLite
} from 'gsap';

import {
    IS_LOADED
} from 'config/events';

export default Vue.extend({

    mixins: [EventManagerMixin],

    domEvents: [],

    appEvents: [],

    data() {

        let pLoad:     number = 0;
        let preloaded: bool = false;
        let animated:  bool = false;

        return {
            style,
            pLoad,
            preloaded,
            animated
        };

    },

    created() {

    },

    mounted() {
        this.$nextTick(() => {
            // code that assumes this.$el is in-document
            this.svgPath = this.$refs.svgpath.getElementsByTagName('path')[0];

            this.createManifest();
            this.preloadAll().then(this.onLoadComplete).catch(this.onLoadError);
        });
    },

    methods: {

        createManifest() {

            this.manifest = [
                require('assets/images/bg/bg.jpg')
            ];

            this.dataJSON = require('assets/datas/statistic-data.json');

        },

        load() {

            return new Promise((resolve, reject) => {

                let queue = new PreloadJs.LoadQueue();

                queue.on('complete', resolve, this);
                queue.on('error', reject, this);
                queue.on('progress', this.onLoadProgress, this);

                queue.loadManifest(this.manifest);

            });

        },

        preloadAll() {
            let ps: any[] = [];

            ps.push(this.load());
            ps.push(Data.load(this.dataJSON));

            return Promise.all(ps);
        },

        onLoadProgress: function (event) {
            let p: number = Math.round(event.progress * 100);

            let fn = () => {

                let val:  number = Math.round(this.pLoad),
                    size: number = 500 * (100 - val) / 100;

                this.$refs.counter.innerHTML = (val < 100 ? '0' + val : val);
                this.svgPath.style.strokeDashoffset = size;

            };

            TweenLite.to(this, 0.3, { pLoad: p, onUpdate: fn, onComplete: () => {
                this.animated = true;
                this.checkProceed();
            } });
        },

        checkProceed: function () {
            if (this.preloaded && this.animated) {
                window.setTimeout(() => {

                    this.emitter.$emit(IS_LOADED, {});

                }, 100);
            }
        },

        onLoadComplete: function () {
            this.preloaded = true;
            this.checkProceed();
        },

        onLoadError: function () {
            console.log('Error while loading files');
        }

    },

    beforeDestroy() {
        TweenLite.killTweensOf(this);
    },

    components: {
        'loaderPath': require('partials/loader')
    }
});
