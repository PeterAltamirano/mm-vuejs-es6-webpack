import style from './sharing.scss';
import EventManagerMixin from 'mixins/EventManagerMixin';
import Platform from 'core/Platform';

import {
    PATH_ROOT
} from 'config/paths';

export default Vue.extend({

    mixins: [EventManagerMixin],

    domEvents: [{
        target: 'dropdown',
        event: 'touchstart',
        method: 'onClick',
        excludeIf: !Platform.isMobile()
    }, {
        target: 'dropdown',
        event: 'mouseenter',
        method: 'onHover',
        excludeIf: Platform.isMobile()
    }, {
        target: 'dropdown',
        event: 'mouseleave',
        method: 'onLeave',
        excludeIf: Platform.isMobile()
    }],

    appEvents: [],

    props: {

    },

    computed: {

    },

    data() {

        let root: string = PATH_ROOT;

        return {
            style,
            root
        };

    },

    watch: {

    },

    created() {
    },

    mounted() {
        this.$nextTick(() => {

        });
    },

    methods: {

        onClick() {
            this.$refs.dropdown.classList.toggle(style.hover);
        },

        onHover() {
            this.$refs.dropdown.classList.add(style.hover);
        },

        onLeave() {
            this.$refs.dropdown.classList.remove(style.hover);
        },

        openWindow(url, w, h) {
            let dualScreenLeft: number = window.screenLeft !== undefined ? window.screenLeft : screen.left,
                dualScreenTop:  number = window.screenTop !== undefined ? window.screenTop : screen.top,
                width:          number = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width,
                height:         number = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            let left: number = ((width / 2) - (w / 2)) + dualScreenLeft,
                top:  number = ((height / 2) - (h / 2)) + dualScreenTop;

            window.open(url, '', 'top=' + top + ',left=' + left + ',width=' + w + ',height=' + h + ',location=no,menubar=no,scrollbars=no');
        },

        shareFacebook() {
            let url: string = this.root;

            this.openWindow('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url), 555, 420);
            return null;
        },

        shareTwitter() {
            let tweet: string = this.$t('general.tweet'),
                url:   string = this.root;

            url = encodeURIComponent(url);
            tweet = encodeURIComponent(tweet);

            this.openWindow('https://twitter.com/intent/tweet?url=' + url + '&text=' + tweet, 550, 420);
            return null;
        }

    },

    components: {

    },

    partials: {

    },

    beforeDestroy() {

    }

});
