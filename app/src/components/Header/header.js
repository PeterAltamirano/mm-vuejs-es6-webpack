import style from './header.scss';
import EventManagerMixin from 'mixins/EventManagerMixin';
import Store from 'store';

import Transitions from 'transitions';

// TODO todo header.js

import {
    PATH_ROOT
} from 'config/paths';

export default Vue.extend({

    mixins: [EventManagerMixin],

    domEvents: [],

    appEvents: [],

    props: {
        loaded: {
            type: Boolean,
            required: true
        }
    },

    computed: {

        route(): string {
            return Store.state.application.route.current;
        }

    },

    data() {

        let root: string = PATH_ROOT;

        return {
            style,
            root
        };

    },

    watch: {

    },

    created() {

    },

    mounted() {
        this.$nextTick(() => {
            // code that assumes this.$el is in-document
            this.bind();
            this.addEventListeners();
        });
    },

    methods: {

        bind() {

        },

        addEventListeners() {

        },

        goBack() {

            this.$router.push({ path: '/' });

        },

        enter(el, done) {

            Transitions.component('header').enter(el, {
                delay: 0.3,
                speed: 0.7,
                ease: Power2.easeInOut
            }, done);

        }

    },

    components: {

    },
    partials: {

    }
});
