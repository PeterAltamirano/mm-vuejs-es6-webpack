import Emitter from 'core/Emitter';

const EventManagerMixin = {

    appEvents: [],

    domEvents: [],

    created() {

        this.emitter = Emitter;

        this.appEvents = this.$options.appEvents;
        this.domEvents = this.$options.domEvents;

        this.bindEMMevents();

    },

    mounted() {
        this.$nextTick(() => {
            this.addEMMeventListeners();
        });
    },

    beforeDestroy() {
        
        if (this.beforeDestroy) {
            this.beforeDestroy();
        }

        this.removeEMMeventListeners();
    },

    methods: {

        bindEMMevents() {

            this.appEvents.forEach((event) => {
                this[event.method] = ::this[event.method];
            });

        },

        addEMMeventListeners() {

            // Add EventEmitter events
            this.appEvents.forEach((appEvent) => {
                if (typeof appEvent.once !== 'undefined') {
                    if (appEvent.once) {
                        this.emitter.$once(appEvent.message, this[appEvent.method]);
                    }
                } else {
                    this.emitter.$on(appEvent.message, this[appEvent.method]);
                }
            });

            // Add DOM events
            this.domEvents.forEach((domEvent) => {

                let t = domEvent.target;

                if (typeof domEvent.target === 'string') {
                    // target element was defined as variable of this.$refs (vuejs elements)
                    t = this.$refs[domEvent.target];
                }
                if (typeof domEvent.target === 'undefined') {
                    t = document;
                }
                if (!domEvent.excludeIf) {
                    t.addEventListener(domEvent.event, this[domEvent.method], false);
                }
            });
        },

        removeEMMeventListeners() {

            // Remove EventEmitter events
            this.appEvents.forEach((appEvent) => {
                this.emitter.$off(appEvent.message, this[appEvent.method]);
            });

            // Remove DOM events
            this.domEvents.forEach((domEvent) => {

                let t = domEvent.target;

                if (typeof domEvent.target === 'string') {
                    t = this.$refs[domEvent.target];
                }
                if (typeof domEvent.target === 'undefined') {
                    t = document;
                }
                if (!domEvent.excludeIf) {
                    t.removeEventListener(domEvent.event, this[domEvent.method], false);
                }
            });
        }
    }
};

module.exports = EventManagerMixin;
