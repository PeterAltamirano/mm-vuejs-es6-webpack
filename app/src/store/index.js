/**
 * Vuex Store
 * @namespace store
 */

import Platform from 'core/Platform';
import Router from 'core/Router';

import Vuex from 'vuex';

Vue.use(Vuex);

const Store = new Vuex.Store({

    state: {
        device: Platform.deviceType,
        browser: Platform.browserName,
        application: {
            route: {
                current: '',
                previous: ''
            }
        },
        data: {

        }
    },

    mutations: {

        /**
         * Update computed route properties
         * @memberof store
         * @function CHANGE_ROUTE
         * @param {Object} route
         */
        CHANGE_ROUTE(state, route) {
            state.application.route.previous = route.from.name;
            state.application.route.current = route.to.name;
        },

        /**
         * Feed the store with default values
         * @memberof store
         * @function SET_DATA
         * @param {Object} data
         */
        SET_DATA(state, data) {
            state.data = data;
        }

    },

    actions: {

        BIND_ROUTE({ commit, state }) {

            commit('CHANGE_ROUTE', {
                from: {
                    name: state.application.route.previous || Router.currentRoute.name
                },
                to: {
                    name: state.application.route.current || Router.currentRoute.name
                }
            });

        },

        CHANGE_ROUTE({ commit, state }, route) {
            commit('CHANGE_ROUTE', route);
        }

    }

});

export default Store;
