<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta id="viewport" name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">

        <title>Vue.js + Webpack</title>
        <meta name="description" content="Boilerplate description">

        <meta property="og:title" content="Vue.js + Webpack"/>
        <meta property="og:type" content="website"/>
        <meta property="og:image" content="{host}/share/main.jpg"/>

        <meta property="og:image:width" content="1200">
        <meta property="og:image:height" content="630">

        <meta property="og:site_name" content="Vue.js + Webpack"/>
        <meta property="og:description" content="Boilerplate description"/>
        <meta property="og:url" content="{host}">

        <meta name="twitter:card" content="summary">
        <meta name="twitter:url" content="{host}">
        <meta name="twitter:title" content="Vue.js + Webpack">
        <meta name="twitter:description" content="Boilerplate description">
        <meta name="twitter:image" content="{host}/share/main.jpg">

        <!-- INSERT GENERATED FAVICONS -->
        <link rel="shortcut icon" href="/favicon.ico">

    <link href="/css/main-55517efdc749e6f3eae1.min.css" rel="stylesheet"></head>

    <body>

        <!-- Google Tag Manager -->
        <!-- <noscript><iframe src="//www.googletagmanager.com/ns.html?id=ADD_GTM_TRACKING_CODE" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','ADD_GTM_TRACKING_CODE');</script> -->
        <!-- End Google Tag Manager -->

        <div id="application">

        </div>

    <script type="text/javascript" src="/js/main-55517efdc749e6f3eae1.min.js"></script></body>

</html>
