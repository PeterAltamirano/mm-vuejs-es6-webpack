<?php

class HttpHelper {
    private static function isHttps()
    {
        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            return true;
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            return true;
        }
        if (!empty($_SERVER['PORT']) && $_SERVER['PORT'] == 443) {
            return true;
        }

        if ((!empty($_SERVER['HTTP_CLOUDFRONT_FORWARDED_PROTO']) && $_SERVER['HTTP_CLOUDFRONT_FORWARDED_PROTO'] === 'https')) {
            return true;
        }

        return false;
    }

    private static function getProtocol()
    {
        return self::isHttps() ? 'https:' : 'http:';
    }

    public static function getHost()
    {
        return self::getProtocol() . '//' . $_SERVER['HTTP_HOST'];
    }
}

?>
