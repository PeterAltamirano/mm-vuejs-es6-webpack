'use strict';

import path from 'path';
import express from 'express';
import webpack from 'webpack';
import history from 'connect-history-api-fallback';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import proxyMiddleware from 'http-proxy-middleware';
import config from './webpack/webpack.dev.config.babel.js';
import { PROXY_TABLE } from '../config/dev.env';

const port = process.env.PORT || 3000;
const ip = process.env.IP || '0.0.0.0';

const app = express();
const compiler = webpack(config);

// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
const proxyTable = PROXY_TABLE;

// proxy api requests
Object.keys(proxyTable).forEach(function (context) {
    let options = proxyTable[context];

    if (typeof options === 'string') {
        options = { target: options };
    }
    app.use(proxyMiddleware(context, options));
});

app.use(history());

app.use(webpackDevMiddleware(compiler, {
    hot: true,
    historyApiFallback: true,
    publicPath: config.output.publicPath,
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,PUT,OPTIONS'
    },
    stats: {
        colors: true,
        hash: true,
        timings: true,
        chunks: false,
        chunkModules: false,
        modules: false
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: true
    }
}));

app.use(webpackHotMiddleware(compiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartbeat: 10 * 1000,
}));

app.use(express.static(path.join(__dirname, '..', 'app', 'static')));

app.get('*', (req, res) => res.sendFile(path.join(__dirname, '..', 'app', 'index.html')));

module.exports = app.listen(port, ip, error => {
    if (error) {
        throw error;
    }

    /* eslint-disable no-console */
    console.info(`Listening on port ${port}. Open up http://${ip}:${port}/ in your browser.`);
    /* eslint-enable no-console */
});
