import path from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import StatsWebpackPlugin from 'stats-webpack-plugin';
import Package from '../../package.json';
import common from './webpack.config.babel.js';

import {
    PUBLIC_PATH_HOST
} from '../../config';

export default merge(common, {

    entry: [
        'babel-polyfill',
        '../app/src/main.js'
    ],

    output: {
        path: path.join(__dirname, '..', '..', 'deploy/htdocs'),
        filename: 'js/[name]-[hash].min.js',
        publicPath: PUBLIC_PATH_HOST
    },

    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            query: 'importLoaders=1&minimize=true&modules=true&localIdentName=[name]__[local]___[hash:base64:5]'
                        },
                        'postcss-loader',
                        'sass-loader',
                        {
                            loader: 'sass-resources-loader',
                            options: {
                                resources: path.resolve(__dirname, '..', '..', 'app', 'src', 'styles', 'resources', '**.scss')
                            }
                        }
                    ],
                    publicPath: '../'
                })
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: '../app/src/template/index.tpl.ejs',
            inject: 'body',
            filename: 'index.tpl',
            title: Package.title,
            description: Package.description
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                /* eslint-disable camelcase */
                drop_console: true,
                pure_funcs: ['console.log']
                /* eslint-disable camelcase */
            }
        }),
        new ExtractTextPlugin({ filename: 'css/[name]-[hash].min.css', allChunks: true }),
        new CleanWebpackPlugin(['*'], { root: path.join(__dirname, '..', '..', 'deploy/') }),
        new StatsWebpackPlugin('../webpack.stats.json')
    ]
});
