const path = require('path');
const webpack = require('webpack');
const precss = require('precss');
const autoprefixer = require('autoprefixer');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

module.exports = {

    context: path.resolve(__dirname, '..'),

    resolve: {
        modules: [
            path.resolve(__dirname, '..', '..', 'app', 'src'),
            'node_modules'
        ],
        alias: {
            'vue$': path.resolve(__dirname, '..', '..', 'node_modules', 'vue', 'dist', 'vue.common.js')
        }
    },

    module: {
        rules: [
            { 
                test: /\.js$/, exclude: /node_modules/,
                enforce: 'pre',
                use: 'eslint-loader' 
            },
            {
                test: /\.html?$/,
                exclude: /node_modules/,
                use: 'html-loader'
            },
            {
                test: /\.vue$/,
                use: {
                    loader: 'vue-loader',
                    options: {
                        transformToRequire: {
                            video: 'src',
                            source: 'src'
                        }
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader'
                ]
            },
            { 
                test: /\.json$/,
                use: {
                    loader: 'file-loader',
                    query: 'name=datas/[name].[ext]'
                }
            },
            { 
                test: /\.(eot|woff(2)?|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: {
                    loader: 'file-loader',
                    query: 'name=fonts/[name].[ext]'
                }
            },
            { 
                test: /\.(png|jpg|gif)$/,
                use: {
                    loader: 'file-loader',
                    query: 'name=images/[name].[ext]'
                }
            },
            { 
                test: /\.(mp3|mp4|webm|ogg)$/,
                use: {
                    loader: 'file-loader',
                    query: 'name=media/[name].[ext]'
                }
            }
        ]
    },

    plugins: [
        new WebpackNotifierPlugin({ alwaysNotify: true }),
        new FriendlyErrorsWebpackPlugin(),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: function () {
                    return {
                        defaults: [precss, autoprefixer],
                        cleaner: [autoprefixer({ browsers: [] })]
                    };
                },
                context: path.resolve(__dirname, '..')
            }
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            '__DEV__': JSON.stringify(process.env.NODE_ENV === 'development'),
            '__PROD__': JSON.stringify(process.env.NODE_ENV === 'production')
        }),
        new webpack.ProvidePlugin({
            'Vue': 'vue'
        }),
        new CopyWebpackPlugin([{
            from: '../app/static'
        }], {
            ignore: ['.DS_Store']
        })
    ],

    stats: { children: false }
};
