const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Package = require('../../package.json');
const common = require('./webpack.config.babel.js');

module.exports = merge(common, {

    devtool: 'inline-source-map',

    entry: [
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true',
        '../app/src/main.js'
    ],

    output: {
        path: __dirname,
        publicPath: '/',
        filename: 'bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                exclude: /node_modules/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        query: 'importLoaders=1&modules=true&localIdentName=[name]__[local]___[hash:base64:5]'
                    },
                    'postcss-loader',
                    'sass-loader',
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: path.resolve(__dirname, './..', '..', 'app', 'src', 'styles', 'resources', '**.scss')
                        }
                    }
                ]
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: '../app/src/template/index.tpl.ejs',
            inject: 'body',
            filename: 'index.html',
            title: Package.title,
            description: Package.description
        }),
        new webpack.HotModuleReplacementPlugin()
    ]

});
