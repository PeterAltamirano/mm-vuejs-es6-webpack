// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {

    before: (browser) => {
        console.log('Setting up...');
    },
    after: (browser) => {
        console.log('All tests done.');
    },

    '__start__': ((browser) =>  browser.url(browser.globals.devServerURL)),

    /**
     * Tests
     */

    'Check wrapper': (browser) => {

        browser
            .waitForElementVisible('body', 2000)
            // .assert.elementPresent('.application__wrapper');
            // .assert.elementCount('a', 1)
            // .assert.containsText('#main', 'Night Watch')
            // .setValue('input[type=text]', 'nightwatch')
            // .click('button[name=btnG]')

    },

    /**
     * End
     */

    '__stop__': ((browser) => browser.end())

};
