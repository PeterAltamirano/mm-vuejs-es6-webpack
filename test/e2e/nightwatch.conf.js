require('babel-register');

const
    SELENIUM_PORT = 4444,
    SELENIUM_HOST = '127.0.0.1',
    DEV_SERVER_HOST = 'localhost',
    DEV_SERVER_PORT = 3000;

// http://nightwatchjs.org/guide#settings-file
module.exports = {
    'src_folders': ['test/e2e/specs'],
    'output_folder': 'test/e2e/reports',
    'custom_assertions_path': ['test/e2e/custom-assertions'],

    'selenium': {
        'start_process': true,
        'server_path': 'node_modules/selenium-server/lib/runner/selenium-server-standalone-3.1.0.jar',
        'host': SELENIUM_HOST,
        'port': SELENIUM_PORT,
        'cli_args': {
            'webdriver.chrome.driver': require('chromedriver').path
        }
    },

    'test_settings': {
        'default': {
            'selenium_port': SELENIUM_PORT,
            'selenium_host': SELENIUM_HOST,
            'silent': true,
            'globals': {
                'devServerURL': 'http://' + DEV_SERVER_HOST + ':' + (process.env.PORT || DEV_SERVER_PORT)
            }
        },

        'chrome': {
            'desiredCapabilities': {
                'browserName': 'chrome',
                'javascriptEnabled': true,
                'acceptSslCerts': true
            }
        },

        'firefox': {
            'desiredCapabilities': {
                'browserName': 'firefox',
                'javascriptEnabled': true,
                'acceptSslCerts': true
            }
        }
    }
};
