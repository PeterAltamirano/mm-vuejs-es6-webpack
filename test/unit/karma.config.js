const webpack = require('../../tools/webpack/webpack.dev.config.babel.js');

module.exports = config => {

    let browsers = ['Chrome'];

    const configuration = {
        browsers,
        singleRun: true,
        frameworks: ['mocha', 'chai'],
        reporters: ['mocha', 'coverage'],
        files: ['./index.js'],
        plugins: [
            'karma-chrome-launcher',
            'karma-phantomjs-launcher',
            'karma-chai',
            'karma-mocha',
            'karma-webpack',
            'karma-coverage',
            'karma-mocha-reporter'
        ],
        preprocessors: {
            './index.js': ['webpack']
        },
        exclude: [
            '/node_modules/'
        ],
        webpack,
        webpackMiddleware: {
            noInfo: false
        },
        customLaunchers: {
            chromeTravisCI: {
                base: 'Chrome',
                flags: ['--no-sandbox']
            }
        },
        coverageReporter: {
            reporters: [
                {
                    type: 'text'
                },
                {
                    type: 'html',
                    dir: 'coverage/'
                }
            ]
        }
    };

    config.set(configuration);
};
