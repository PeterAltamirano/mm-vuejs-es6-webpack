// /* globals expect describe */

const expect = require('chai').expect;
const should = require('chai').should();

import Application from '../../../app/src/containers/Application/application.vue';
import Loader from '../../../app/src/components/Loader/loader.vue';

describe('Vue', () => {

    it('#Application: vm should be an object ', () => {
        
        const vm = new Vue({
            el: document.createElement('div'),
            render: (h) => h(Application)
        });

        expect(vm).to.be.a('object');
    });

    it('#Loader: can launch the app', (done) => {
        
        const vm = new Vue({
            template: '<div><loader ref="loader"></loader></div>',
            components: {
                'loader': Loader
            }
        }).$mount();

        Vue.nextTick(() => {
            
            expect(vm.$el.querySelectorAll('.a-loader').length).equal(1);
            should.exist(vm.$refs.loader.onLoadComplete);
            
            done();
        });

    });

});

